<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Connexion</title>
</head>
<body>

<h1>Connectez-vous !</h1>

	<p>Vous n'êtes pas membre ? <a href="${pageContext.request.contextPath}/inscription">Inscrivez vous !</a></p>

	<p><%=(request.getAttribute("db_error") == null) ? ""
	 : request.getAttribute("db_error")%></p>
	 
	 <p>${db_error}</p>

	<form action="${pageContext.request.contextPath}/connexion" method="post" name="connexion">
		
		<fieldset>
                <legend>Connexion</legend>

                <p>Veuillez saisir vos données de compte.</p>

                <label for="nom">Nom d'utilisateur</label>
                <input type="text" id="nom" name="nom" value="<c:out value="${ATT_USER.NomUtilisateur}"/>" size="20" maxlength="20" />
                <span class="erreur">${ATT_ERREURS['ERR_NOM']}</span>
                <br />

                <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="mdp" name="mdp" value="" size="20" maxlength="20" />
                <span class="erreur">${ATT_ERREURS['ERR_MDP']}</span>
                <br />
                
            </fieldset>
		
		<input type="reset" name="reinitialiser" value="Réinitialiser">
		<input type="submit" name="envoyer" value="Envoyer">
	</form>
	
	<c:if test="${!empty sessionScope.ATT_SESSION_USER}">

                    <%-- Si l'utilisateur existe en session, alors on affiche son adresse email. --%>

                    <p class="succes">Vous êtes connecté(e) avec l'adresse : ${sessionScope.ATT_SESSION_USER.email}</p>

                </c:if>


</body>
</html>