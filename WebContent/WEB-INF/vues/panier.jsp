<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Panier</title>
</head>
<body>

<h1>Bienvenue <c:out value="${ATT_SESSION_USER.nomUtilisateur}"/></h1>
	
	<p>Votre groupe : <c:out value="${ATT_SESSION_USER.groupe}"/></p>
	
	<c:if test="${!empty sessionScope.ATT_SESSION_USER}">
                    <p class="succes">Vous êtes connecté(e) avec l'adresse : ${sessionScope.ATT_SESSION_USER.email}</p>
    </c:if>
    
    <h2>Votre panier : <c:out value="${ATT_PANIER.id}"/></h2>
    
    <c:choose>
	    <c:when test="${!empty ATT_PANIER.panier }">
	    <table>
	    <tr>
	      <th>Nom</th>
	      <th>Prix</th>
	    </tr>
	      <c:forEach items="${ATT_PANIER.panier}" var="item">
	        <tr>
	          <td><c:out value="${item.nom}" /><td>
	          <td><c:out value="${item.prix}" /><td>
	        </tr>
	      </c:forEach>
	    </table>
	    </c:when>
	    <c:otherwise>
	    	<p><c:out value="Votre panier est présentement vide" /></p>
	    </c:otherwise>
	</c:choose>

</body>
</html>