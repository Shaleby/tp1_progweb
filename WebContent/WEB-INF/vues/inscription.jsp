<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inscription</title>
</head>
<body>

<h1>Veuillez remplir ce formulaire</h1>

	<p><%=(request.getAttribute("db_error") == null) ? ""
	 : request.getAttribute("db_error")%></p>
	 
	 <p>${db_error}</p>

	<form action="${pageContext.request.contextPath}/inscription" method="post" name="inscription">
		
		<fieldset>
                <legend>Inscription</legend>

                <p>Vous pouvez vous inscrire via ce formulaire.</p>

                <label for="nom">Nom d'utilisateur</label>
                <input type="text" id="nom" name="nom" value="<c:out value="${ATT_USER.nomUtilisateur}"/>" size="20" maxlength="20" />
                <span class="erreur">${ATT_ERREURS['ERR_NOM']}</span>
                <br />
                
                <label for="email">Adresse email <span class="requis">*</span></label>
                <input type="text" id="email" name="email" value="<c:out value="${ATT_USER.email}"/>" size="20" maxlength="60" />
                <span class="erreur">${ATT_ERREURS['ERR_EMAIL']}</span>
                <br />

                <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                <input type="password" id="mdp" name="mdp" value="" size="20" maxlength="20" />
                <span class="erreur">${ATT_ERREURS['ERR_MDP']}</span>
                <br />

                <label for="confirmation">Confirmation du mot de passe <span class="requis">*</span></label>
                <input type="password" id="cmdp" name="cmdp" value="" size="20" maxlength="20" />
                <span class="erreur">${ATT_ERREURS['ERR_MDP']}</span>
                <br />
                
                <label for="adresse">Adresse <span class="requis">*</span></label>
                <input type="text" id="adresse" name="adresse" value="${ATT_USER.adresse}" size="100" maxlength="100" />
                <span class="erreur">${ATT_ERREURS['ERR_ADRESSE']}</span>
                <br />
                
                <label for="maxDistance">Distance <span class="requis">*</span></label>
                <input type="text" id="distance" name="distance" value="${ATT_USER.distance}" size="10" maxlength="10" />
                <span class="erreur">${ATT_ERREURS['ERR_DISTANCE']}</span>
                <br />
                
            </fieldset>
		
		<input type="reset" name="reinitialiser" value="Réinitialiser">
		<input type="submit" name="envoyer" value="Envoyer">
	</form>

</body>
</html>