package com.garneau.java.web.bean;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.garneau.java.web.bean.BeanGroupe;
import com.garneau.java.web.bean.BeanUtilisateur;
import com.garneau.java.web.dao.GroupeDao;
import com.garneau.java.web.dao.UtilisateurDao;

public class ModeleFormulaire {
	
	/* map des erreurs de champs du formulaire */
	private Map<String, String> erreurs = new HashMap<String, String>();
	/* statut d'erreur */
	private boolean status = false;
	
	public Map<String, String> getErreurs() {
		return erreurs;
	}
	
	public boolean getStatus() {
		return status;
	}
	
	/**
	 * Validation des données de formulaire à l'inscription
	 * 
	 * @param request : pour accès aux paramètres de champs du formulaire : nom, email, mot-de-passe, confirmation du mot-de-passe, adresse et valeur de distance
	 * @return : un BeanUtilisateur correctement constitué
	 */
	public BeanUtilisateur inscription(HttpServletRequest request) {

		BeanUtilisateur user = new BeanUtilisateur();

		String nom = request.getParameter("nom");
		String email = request.getParameter("email");
		String mdp = request.getParameter("mdp");
		String cmdp = request.getParameter("cmdp");
		String adresse = request.getParameter("adresse");
		String distance = request.getParameter("distance");

		try {
			validationNom(nom);
		} catch (Exception e) {
			erreurs.put("ERR_NOM", e.getMessage());
			status = true;
		}
		

		try {
			validationEmail(email);
		} catch (Exception e) {
			erreurs.put("ERR_EMAIL", e.getMessage());
			status = true;
		}
		

		try {
			validationMdp(mdp, cmdp);
		} catch (Exception e) {
			erreurs.put("ERR_MDP", e.getMessage());
			status = true;
		}
		

		try {
			validationAdresse(adresse);
		} catch (Exception e) {
			erreurs.put("ERR_ADRESSE", e.getMessage());
			status = true;
		}
		

		try {
			validationDistance(distance);
		} catch (Exception e) {
			erreurs.put("ERR_DISTANCE", e.getMessage());
			status = true;
		}
		

		/* En cas de succès, attacher le nouvel inscrit à un groupe */
		if (status == false) {
			
			user.setNomUtilisateur(nom);
			user.setEmail(email);
			user.setMotDePasse(mdp);
			user.setAdresse(adresse);
			user.setDistance(Integer.valueOf(distance));
			
			GroupeDao gDao = new GroupeDao();

			/* obtenir une clé de groupe */
			int groupeId = gDao.nearestGroupe(user);

			/* attacher l'id de groupe au bean utilisateur */
			user.setGroupe(groupeId);

		}

		return user;

	}

	/**
	 * Validation des données de formulaire à la connexion : nom seul
	 * 
	 * @param request : l'objet requête comprenant le nom et le mot de passe de l'utilisateur
	 */
	public BeanUtilisateur connexion(HttpServletRequest request) {
		
		BeanUtilisateur user = new BeanUtilisateur();
		

		// TODO : Validation du formulaire de connexion
		
		user.setNomUtilisateur("matt");
		user.setEmail("matt@ccc.com");
		user.setId(8);
		user.setGroupe(1);
		
		return user;
		
	}
	
	/*
	 * ########################### UTILS ###########################
	 */
	
	/**
	 * Validation du champs email
	 * 
	 * @param email
	 * @throws Exception : message d'erreur
	 */
	private void validationEmail(String email) throws Exception {
		if ( email != null && email.trim().length() != 0 ) {
	        if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
	            throw new Exception("Merci de saisir une adresse mail valide.");
	        }
	    } else {
	        throw new Exception("Merci de saisir une adresse mail.");
	    }
	}
	
	/**
	 * Validation du champ nom
	 * 
	 * @param nom
	 * @throws Exception : message d'erreur
	 */
	private void validationNom(String nom) throws Exception {
		if ( nom != null && nom.trim().length() < 3 ) {
	        throw new Exception("Un nom d'utilisateur doit contenir au moins 3 caractères.");
	    }
	}
	
	/**
	 * Validation du champ mot-de-passe
	 * 
	 * @param mdp : mot-de-passe
	 * @param cmdp : confirmation du mot-de-passe
	 * @throws Exception : message d'erreur
	 */
	private void validationMdp(String mdp, String cmdp) throws Exception {
		if (mdp != null && mdp.trim().length() != 0 && cmdp != null && cmdp.trim().length() != 0) {
	        if (!mdp.equals(cmdp)) {
	            throw new Exception("Les mots de passe entrés ne correspondent pas, veuillez les saisir à nouveau.");
	        } else if (mdp.trim().length() < 3) {
	            throw new Exception("Un mot de passe doit contenir au moins 3 caractères.");
	        }
	    } else {
	        throw new Exception("Merci de saisir et confirmer votre mot de passe.");
	    }
	}
	
	/**
	 * Validation du champ adresse
	 * 
	 * @param adresse
	 * @throws Exception : message d'erreur
	 */
	private void validationAdresse(String adresse) throws Exception {
		if (adresse == null) {
			throw new Exception("Merci de saisir votre adresse.");
		}
	}
	
	/**
	 * Validation de la valeur de distance utilisée pour recherche de groupe
	 * 
	 * @param distance
	 * @throws Exception : message d'erreur
	 */
	private void validationDistance(String distance) throws Exception {
		if (!distance.matches("[0-9]+")) {
			throw new Exception("Merci de saisir une valeur de distance en kms");
		}
	}
	

}
