package com.garneau.java.web.bean;

public class BeanUtilisateur {
	
	private int id;
	private String email;
	private String nomUtilisateur;
	private String motDePasse;
	private String adresse;
	private int groupe;
	
	private int distance;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	
	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}
	
	public String getMotDePasse() {
		return motDePasse;
	}
	
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public String getAdresse() {
		return adresse;
	}
	
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	public int getGroupe() {
		return groupe;
	}
	
	public void setGroupe(int groupe) {
		this.groupe = groupe;
	}
	
	public int getDistance() {
		return distance;
	}
	
	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	public String toString() {
		String show = new String();
		
		show = "Nom : " + nomUtilisateur + " / mdp : " + motDePasse + " / adresse : " + adresse;
		return show;
	}

}
