package com.garneau.java.web.bean;

public class BeanGroupe {
	
	private int id;
	private String adresse;
	private int idGerant;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	public int getGerant() {
		return idGerant;
	}
	public void setGerant(int idGerant) {
		this.idGerant = idGerant;
	}

}
