package com.garneau.java.web.bean;

import java.util.ArrayList;

import com.garneau.java.web.bean.BeanItem;

public class BeanPanier {
	
	/* identifiant du propriétaire du panier : id de UserData */
	private int id;
	/* liste d'items */
	private ArrayList<BeanItem> panier;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public ArrayList<BeanItem> getPanier() {
		return panier;
	}
	public void setPanier(ArrayList<BeanItem> panier) {
		this.panier = panier;
	}

}
