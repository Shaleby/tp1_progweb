package com.garneau.java.web.controler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.garneau.java.web.bean.ModeleFormulaire;
import com.garneau.java.web.dao.UtilisateurDao;
import com.garneau.java.web.dao.PanierDao;
import com.garneau.java.web.bean.BeanUtilisateur;

/**
 * Servlet implementation class ControleurGeneral
 */
@WebServlet("/ControleurGeneral")
public class ControleurGeneral extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControleurGeneral() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String uri = request.getRequestURI();
		String pageValue = uri.substring(uri.lastIndexOf('/') + 1);
		
		if (pageValue.equals("inscription")) {
			request.getRequestDispatcher("/WEB-INF/vues/inscription.jsp").forward(request, response);
		}
		
		else if (pageValue.equals("connexion")) {
			request.getRequestDispatcher("/WEB-INF/vues/connexion.jsp").forward(request, response);
		}
		
		/*
		 * Cas de deconnexion
		 */
		else if (pageValue.equals("deconnexion")) {

			// TODO deconnexion : retour à la page d'accueil
			
		}
		
		else {
			request.getRequestDispatcher("/WEB-INF/vues/accueil.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ModeleFormulaire form = new ModeleFormulaire();
		BeanUtilisateur user = new BeanUtilisateur();
		
		/* Récupération de la session */
		HttpSession session = request.getSession();
		
		String uri = request.getRequestURI();
		String pageValue = uri.substring(uri.lastIndexOf('/') + 1);

		/* 
		 * Cas de connexion :
		 * différent du cas d'inscription : il peut y avoir un échec de connexion si l'utilisateur n'est pas reconnu
		 * on utilise le formulaire pour placer une erreur si il y a échec de connexion
		 * VOIR UtilisateurDao.connectUtilisateur
		 */
		if (pageValue.equals("connexion")) {
			
			// TODO Mettre en place la procédure de connexion
			
			user = form.connexion(request);
			
			session.setAttribute("ATT_SESSION_USER", user);
			
			response.sendRedirect(request.getContextPath()+"/membre");
			
		}
		
		/* 
		 * Cas d'inscription 
		 */
		else {

			/* validation du formulaire et création d'un bean utilisateur */
			user = form.inscription(request);
			
			/* formulaire correctement rempli */
			if (form.getStatus() == false) {
				
				/* insertion du nouvel inscrit */
				UtilisateurDao uddao = new UtilisateurDao();
				int id = uddao.insertUtilisateur(user);
				
				if (id != 0) {
					user.setId(id);
					
					/* ajout à la session */
					session.setAttribute("ATT_SESSION_USER", user);
					
					/* création d'un panier dans la BD */
					//PanierDao pddao = new PanierDao();
					//pddao.fairePanier(user.getId(), user.getGroupe());
					
					/* routage vers la ressource membre */
					response.sendRedirect(request.getContextPath()+"/membre");
				}
				
				/*else {
					// TODO cas d'erreur à l'insertion MySQL
				}*/
			}
			
			/* cas d'erreur de formulaire, retour vers la page d'inscription */
			else {
				
				session.setAttribute("ATT_SESSION_USER", null);
				
				request.setAttribute("ATT_ERREURS", form.getErreurs());
				/* attachement à la requête du bean utilisateur : permettra le remplissage automatique des champs lors du retour au formulaire */
				request.setAttribute("ATT_USER", user);
				request.getRequestDispatcher("/WEB-INF/vues/inscription.jsp").forward(request, response);
			}
			
		}
		
	}

}
