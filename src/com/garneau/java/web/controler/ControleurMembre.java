package com.garneau.java.web.controler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.garneau.java.web.bean.BeanUtilisateur;
import com.garneau.java.web.bean.BeanPanier;
import com.garneau.java.web.dao.PanierDao;

/**
 * Servlet implementation class ControleurMembre
 */
@WebServlet("/ControleurMembre")
public class ControleurMembre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControleurMembre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* récupération des données utilisateur depuis la session */
		HttpSession session = request.getSession();
		BeanUtilisateur user = (BeanUtilisateur) session.getAttribute("ATT_SESSION_USER");
		
		/* 
		 * vérification de la connexion utilisateur via le BeanUtilisateur enregistré en session
		 * Si connecté, alors on va vers la vue membre
		 */
		if (user != null) {
			String uri = request.getRequestURI();
			String pageValue = uri.substring(uri.lastIndexOf('/') + 1);
			
			/* consultation du panier */
			if (pageValue.equals("panier")) {
				/* récupération du panier */
				PanierDao pddao = new PanierDao();
				BeanPanier panier = pddao.getPanier(user.getId());
				/* attachement du BeanPanier à la requête */
				request.setAttribute("ATT_PANIER", panier);
				/* envoi de la requête */
				request.getRequestDispatcher("/WEB-INF/vues/panier.jsp").forward(request, response);
			}
			
			else if (pageValue.equals("message")) {
				
				// TODO mettre en place la ressource permettant affichage des messages
				
			}
			
			else if (pageValue.equals("suppr-message")) {
				
				/* 
				 * TODO mettre en place la ressource permettant la suppression d'un message
				 * Supprimer en BD
				 * Vider la liste des messages
				 * Retour à la vue /membre
				 */
				
			}
			
			else {
				
				// TODO Vérifier la boîte aux lettres : nécessaire parce que l'affichage en vue /membre d'un lien vers /membre/message sera conditionnel à l'existence de message(s)
				
				request.getRequestDispatcher("/WEB-INF/vues/membre.jsp").forward(request, response);
			}
		}
		
		/* 
		 * utilisateur non connecté
		 * on retourne vers connexion
		 */
		else {
			response.sendRedirect(request.getContextPath()+"/connexion");
		}
		
	}

}
