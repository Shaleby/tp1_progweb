package com.garneau.java.web.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	public static Connection createConnection() {
		Connection con = null;
		/* Url MySQL suivi du nom de BD */
		String url = "jdbc:mysql://localhost:3306/bdd_demo02e";
		/* Nom d'utilisateur MySQL */
		String username = "garneau";
		/* Mot de passe MySQL */
		String password = "1234";
	 
	try {
		/* chargement du driver MySQL */
	    try {
	        Class.forName("com.mysql.jdbc.Driver");
	    } catch (ClassNotFoundException e) {
	    	e.printStackTrace();
	    }
	 
	    con = DriverManager.getConnection(url, username, password); //attempting to connect to MySQL database
	    System.out.println("Instance de connexion : "+con);
	 } 
	 catch (SQLException e) {
	     e.printStackTrace();
	 }
	 
	 return con; 
	 }
	 
	 public static void closeConnection(Connection con) {
		 if ( con != null )
		    try {
		    /* Fermeture de la connexion */
		    con.close();
		    } catch ( SQLException ignore ) {
		    /* Erreur survient lors de la fermeture. */
		    }
	 	  }
	 

}
