package com.garneau.java.web.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.garneau.java.web.bean.BeanGroupe;
import com.garneau.java.web.bean.BeanUtilisateur;
import com.garneau.java.web.util.DBConnection;
import com.garneau.java.web.bean.BeanGroupe;
import com.garneau.java.web.util.GeoDistance;

public class GroupeDao {
	
	/**
	 * Méthode d'insertion d'un nouveau groupe dans la BD
	 * 
	 * @param groupe
	 * @return
	 */
	public int insertGroupe(BeanGroupe groupe) {
		
		String adresse = groupe.getAdresse();
		//int idGerant = groupe.getGerant();
		
		/* Set renvoyé par MySQL : retournera la clé de l'enregistrement */
		ResultSet genKey;
		
		Connection con = null;
		/* Objet conteneur du résultat de la requête : requête préparée */
		PreparedStatement preparedStatement = null;
		 
		try {
			con = DBConnection.createConnection();
			String query = "insert into groupes(Id, adresse) values (NULL,?)";
			//String query = "insert into groupes(Id, adresse, id_gerant) values (NULL,?,?)";
			
			/* le second argument de prepareStatement spécifie que l'on veut récupérer les clés créées à l'insertion */
			preparedStatement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			
			/* placement des valeurs dans la requête préparée */
			preparedStatement.setString(1, adresse);
			//preparedStatement.setInt(2, idGerant);

			/* executeUpdate : méthode d'exécution d'insert */
			preparedStatement.executeUpdate();

			/* récupération de la clé */
			genKey = preparedStatement.getGeneratedKeys();  
			int key = genKey.next() ? genKey.getInt(1) : 0;

			preparedStatement.close();
			con.close();

			return key;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		/* En cas d'échec à la connexion avec la BD */;
		return 0;
		
	}
	
	/**
	 * Méthode de récupération du groupe le plus proche
	 * 
	 * @param adresse : adresse du postulant
	 * @param maxDistance : valeur de distance maximale souhaitée par le postulant
	 * @return
	 */
	public int nearestGroupe(BeanUtilisateur user) {
		
		String adresse = user.getAdresse();
		//int id = user.getId();
		int maxDistance = user.getDistance();
		
		/* Récupération d'une liste contenant tous les groupes de moins de 10 membres */
		ArrayList<BeanGroupe> listGroupes = this.getAllGroupes(true);
		
		BeanGroupe candidGroupe = null;
		int groupeKey;
		
		GeoDistance geo = new GeoDistance();
		
		/* comparaison de distance entre les adresses des groupes et de l'utilisateur */
		for (BeanGroupe groupe : listGroupes) {
				Double distance = geo.compareDistance(adresse, groupe.getAdresse());
				Double max = Double.valueOf(maxDistance);
				if (distance < max) {
					candidGroupe = groupe;
					max = distance;
				}
		}
		
		/* si il existe un groupe proche et incomplet */
		if (candidGroupe != null) {
			groupeKey = candidGroupe.getId();
		}
		
		/* sinon créer un nouveau groupe */
		else {
			BeanGroupe newGroupe = new BeanGroupe();
			newGroupe.setAdresse(adresse);
			//newGroupe.setGerant(id);
			/* récupération d'un id de groupe à l'insertion en BD */
			groupeKey = insertGroupe(newGroupe);
		}
		
		return groupeKey;
		
	}
	
	/**
	 * Méthode de récupération de l'ensemble des groupes
	 * 
	 * Conditionnel au paramètre :
	 * @param incomplet : booléen
	 * 		- true : ne renvoyer que les groupes de moins de 10 membres
	 * 		- false : renvoyer tous les groupes
	 * @return
	 */
	public ArrayList<BeanGroupe> getAllGroupes(boolean ouvert) {
		
        ArrayList<BeanGroupe> listGroupes = new ArrayList<BeanGroupe>();
        
        /* Set renvoyé par MySQL : retournera une liste des groupes, partielle ou totale selon les cas */
        ResultSet resultats = null;
        
		Connection con = null;
		/* Objet conteneur du résultat de la requête : requête préparée */
		PreparedStatement preparedStatement = null;
		 
		try {
			 con = DBConnection.createConnection();
			 String query = "select * from groupes";
			 preparedStatement = con.prepareStatement(query);
			 
			 /* executeQuery : méthode d'exécution de select : retourne un ResultSet */
			 resultats = preparedStatement.executeQuery();
			 
			 /* cas 1 : groupe de moins de membres */
			 if (ouvert == true) {
				 
				 Map<Integer, Integer> nbrMembresParGroupe = this.nbrMembresGroupe();
				 
				 /* parcours du ResultSet */
				 while(resultats.next()) {
					 int id = resultats.getInt("id");
					 // récupérer le nombre de membres pour chaque groupe
					 int nbrMembres = nbrMembresParGroupe.get(id);

					 /* 
					  * ajouter à la liste seulement les groupes de moins de 10 membres
					  * note : on ne récupère que l'id et l'adresse du groupe, car on a pas besoin de l'id du gérant ici
					  */
					 if (nbrMembres<10) {
						 BeanGroupe groupe = new BeanGroupe();
						 groupe.setAdresse(resultats.getString("adresse"));
						 groupe.setId(id);

						 listGroupes.add(groupe);
					 }
				 }
			 }
			 
			 /* cas 2 : tous les groupes */
			 else {
				 /* parcours du ResultSet */
				 while(resultats.next()) {
					 BeanGroupe groupe = new BeanGroupe();
					 groupe.setAdresse(resultats.getString("adresse"));
					 //groupe.setGerant(resultats.getInt("id_gerant"));
					 groupe.setId(resultats.getInt("id"));

					 listGroupes.add(groupe);
				 }
			 }
			 preparedStatement.close();
			 con.close();
			 
		} catch(SQLException e) {
			 e.printStackTrace();
		}
		return listGroupes;
	}
	
	
	/**
	 * Méthode de récupération du nombre de membres par groupe
	 * 
	 * @return : une Map de structure clé=id de groupe, valeur=nombre de membres
	 */
	public Map<Integer, Integer> nbrMembresGroupe() {
		
		Map<Integer, Integer> nbrMembres = null;
		
		/* Set renvoyé par MySQL : retournera les valeurs de champs de la table de transition (groupe-->Nombre de membres) */
		ResultSet resultats = null;
		
		Connection con = null;
		/* Objet conteneur du résultat de la requête : requête préparée */
		PreparedStatement preparedStatement = null;
		
		try {
			 con = DBConnection.createConnection();
			 String query = "SELECT groupe, count(*) as num FROM general GROUP BY groupe";
			 preparedStatement = con.prepareStatement(query);
			 
			 /* executeQuery : méthode d'exécution de select : retourne un ResultSet */
			 resultats = preparedStatement.executeQuery();
			 nbrMembres = new HashMap<Integer, Integer>();
			 
			 /* parcours du ResultSet */
			 while(resultats.next()) {
				 int id = resultats.getInt("groupe");
				 
				 /* récupérer le nombre de membres pour chaque groupe */
				 int nombre = resultats.getInt("num");
				 nbrMembres.put(id, nombre);
			 }
			 preparedStatement.close();
			 con.close();
			 
		} catch(SQLException e) {
			 e.printStackTrace();
		}
		
		return nbrMembres;
	}
	

}
