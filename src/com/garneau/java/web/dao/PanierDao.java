package com.garneau.java.web.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.garneau.java.web.bean.BeanItem;
import com.garneau.java.web.bean.BeanPanier;
import com.garneau.java.web.util.DBConnection;

public class PanierDao {
	
	/**
	 * Méthode de récupération du panier et des items contenus
	 * 
	 * @param user : id de l'utilisateur
	 * @return
	 */
	public BeanPanier getPanier(int user) {
		
		BeanPanier panier = null;
		
		/* Set renvoyé par MySQL : retournera les valeurs de champs nécessaires à la récupération du panier : id de panier et items contenus */
		ResultSet resultats = null;
		
		Connection con = null;
		/* Objet conteneur du résultat de la requête : requête préparée */
		PreparedStatement preparedStatement = null;
		
		ArrayList<BeanItem> items = new ArrayList<BeanItem>();
		
		try {
			
			con = DBConnection.createConnection();
			/* Requête sur une jointure entre table item et table panier */
			String query = "select item.nom, item.prix, panier.id from item inner join panier where panier.id=? and panier.item=item.Id";
			preparedStatement = con.prepareStatement(query);
			
			/* placement des valeurs dans la requête préparée */
			preparedStatement.setInt(1, user);
			
			/* executeQuery : méthode d'exécution de select : retourne un ResultSet */
			resultats = preparedStatement.executeQuery();
			
			panier = new BeanPanier();
			panier.setId(user);
			
			/* parcours du ResultSet afin de récupérer les valeurs demandées */
			while(resultats.next()) {
				BeanItem item = new BeanItem();
				item.setNom(resultats.getString("nom"));
				item.setPrix(resultats.getString("prix"));
				items.add(item);
			}
			
			panier.setPanier(items);
			
			con.close();
			preparedStatement.close();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return panier;
		
	}
	
	/**
	 * Méthode de création de panier dans la BD
	 * 
	 * @param user : id de l'utilisateur
	 * @param groupe : id du groupe
	 */
	public void fairePanier(int user, int groupe) {
		
		BeanPanier panier = null;
		
		Connection con = null;
		/* Objet conteneur du résultat de la requête : requête préparée */
		PreparedStatement preparedStatement = null;
		
		try {
			con = DBConnection.createConnection();
			String query = "insert into panier(id, item) values (?,?)";
			preparedStatement = con.prepareStatement(query);
			
			/* placement des valeurs dans la requête préparée */
			preparedStatement.setInt(1, user);
			/* !!! Comme la fonctionnalité de remplissage de panier n'est pas active côté client : la valeur d'item est à 0 */
			preparedStatement.setInt(2, 0);
			
			/* executeUpdate : méthode d'exécution d'insert */
			preparedStatement.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		panier = new BeanPanier();
		
		panier.setId(user);
		
		/* ajout d'un panier vide */
		ArrayList<BeanItem> items = new ArrayList<BeanItem>();
		panier.setPanier(items);
		
	}

}
