package com.garneau.java.web.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.garneau.java.web.bean.BeanUtilisateur;
import com.garneau.java.web.bean.ModeleFormulaire;
import com.garneau.java.web.util.DBConnection;

public class UtilisateurDao {
	
	/**
	 * 
	 * @param user : BeanUtilisateur
	 * @return : clé d'enregistrement : pour affectation à la variable id du BeanUtilisateur
	 */
	public int insertUtilisateur(BeanUtilisateur user) {
		
		System.out.println("BeanUtilisateur parvenu dans UtilisateurDao : "+user.toString());
		
		String email = user.getEmail();
		String nom = user.getNomUtilisateur();
		String mdp = user.getMotDePasse();
		String adresse = user.getAdresse();
		int groupe = user.getGroupe();
		
		/* Set renvoyé par MySQL : retournera la clé de l'enregistrement */
		ResultSet genKey;
		
		Connection con = null;
		
		/* Objet conteneur du résultat de la requête : requête préparée */
		PreparedStatement preparedStatement = null;
		
		try {
			 con = DBConnection.createConnection();
			 String query = "insert into general(Id,email,nom,mdp,adresse,groupe) values (NULL,?,?,?,?,?)";
			 
			 /* le second argument de prepareStatement spécifie que l'on veut récupérer les clés créées à l'insertion */
			 preparedStatement = con.prepareStatement(query, preparedStatement.RETURN_GENERATED_KEYS); 
			 
			 /* placement des valeurs dans la requête préparée */
			 preparedStatement.setString(1, email);
			 preparedStatement.setString(2, nom);
			 preparedStatement.setString(3, mdp);
			 preparedStatement.setString(4, adresse);
			 preparedStatement.setInt(5, groupe);
			 
			 preparedStatement.executeUpdate();
			 
			 /* Récupération et parcours du set renvoyé par l'opération SQL insert */
			 genKey = preparedStatement.getGeneratedKeys();  
			 /* Pas d'ambiguité : le set contient une seule clé */
			 int key = genKey.next() ? genKey.getInt(1) : 0;
			 
			 /* Fermeture du statement et de la connexion */
			 preparedStatement.close();
			 con.close();
			 
			 System.out.println("Dans UserDataDao, état de la clé d'insert retourné : "+key);
			 
			 return key;
			 
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	/**
	 * 
	 * @param form : le formulaire contenant à ce stade :
	 * 		- une table d'erreur vide
	 * 		- un statut d'erreur à false
	 * @param user : un BeanUtilisateur partiellement rempli
	 * @return : Un tableau à deux valeurs : 
	 * 		1/ BeanUtilisateur reconstitué
	 * 		2/ le formulaire
	 */
	public Object[] connectUtilisateur(ModeleFormulaire form, BeanUtilisateur user) {
		
			/* table de retour */
			Object[] tableConnexion = new Object[2];
			
			/* Set renvoyé par MySQL : retournera les valeurs de champs de l'enregistrement Utilisateur */
			ResultSet resultats = null;
			
			Connection con = null;
			/* Objet conteneur du résultat de la requête : requête préparée */
			PreparedStatement preparedStatement = null;
			 
			// TODO Mettre en place la validation à la connexion en BD
			
			return tableConnexion;
			
		}

}
